<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

if (PHP_SAPI === 'cli') {
  fputs(STDERR, 'This script is not intended to be run from the command line.');
  fputs(STDERR, "\n");
  fputs(STDERR, 'You should install it on your web server and view it in your web browser.');
  fputs(STDERR, "\n");
  exit;
}

// add headers to prevent caching - https://www.php.net/manual/en/function.session-cache-limiter.php
header( 'Expires: Thu, 19 Nov 1981 08:52:00 GMT' );
header( 'Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0' );
header( 'Pragma: no-cache' );

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex, nofollow">
<title>OPcache Tuner</title>
<link rel="stylesheet" href="bootstrap.css">
<link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="container">
    <h1>OPcache Tuner</h1>
    <?php

    function h($s) {
      return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
    }

    function human_readable_time($seconds) {
      if ($seconds < 60) {
        if ($seconds == 1) {
          $result = '1 second';
        }
        else {
          $result = sprintf('%d seconds', $seconds);
        }
      }
      elseif ($seconds < 3600) {
        $minutes = floor($seconds / 60);
        if ($minutes == 1) {
          $result = '1 minute';
        }
        else {
          $result = sprintf('%d minutes', $minutes);
        }

        $remaining_seconds = $seconds - 60 * $minutes;
        if ($remaining_seconds > 0) {
          $result = sprintf('%s, %s', $result, human_readable_time($remaining_seconds));
        }
      }
      elseif ($seconds < 86400) {
        $hours = floor($seconds / 3600);
        if ($hours == 1) {
          $result = '1 hour';
        }
        else {
          $result = sprintf('%d hours', $hours);
        }

        $remaining_seconds = $seconds - 3600 * $hours;
        if ($remaining_seconds > 0) {
          $result = sprintf('%s, %s', $result, human_readable_time($remaining_seconds));
        }
      }
      else {
        $days = floor($seconds / 86400);
        if ($days == 1) {
          $result = '1 day';
        }
        else {
          $result = sprintf('%d days', $days);
        }

        $remaining_seconds = $seconds - 86400 * $days;
        if ($remaining_seconds > 0) {
          $result = sprintf('%s, %s', $result, human_readable_time($remaining_seconds));
        }
      }
      return $result;
    }

    function box($heading, $message, $type) {
      switch ($type) {
      case 'danger':
        $symbol = '&#x274c;';
        break;
      case 'warning':
        $symbol = '&#x26a0;';
        break;
      case 'success':
        $symbol = '&#x2714;';
        break;
      }
      ?>
      <div class="alert alert-<?= $type ?>">
        <div class="opcachetuner-box">
          <div class="opcachetuner-box-symbol">
            <?= $symbol ?>
          </div>
          <div>
            <h2><?= $heading ?></h2>
            <?= $message ?>
          </div>
        </div>
      </div>
      <?php
    }

    function red($heading, $message) {
      box($heading, $message, 'danger');
    }

    function yellow($heading, $message) {
      box($heading, $message, 'warning');
    }

    function green($heading, $message) {
      box($heading, $message, 'success');
    }

    function strong($s) {
      return '<strong>' . $s . '</strong>';
    }

    try {
      // check the Server API
      $heading = 'Server API';
      $message = 'Your server API is <code>' . h(PHP_SAPI) . '</code>.';
      if (PHP_SAPI === 'fpm-fcgi') {
        green($heading, $message);
      }
      elseif (in_array(PHP_SAPI, ['apache', 'apache2handler'], TRUE)) {
        $message .= "\n";
        $message .= 'For best results it is recommended to run PHP using <a rel="noreferrer" href="https://www.php.net/manual/en/install.fpm.php">PHP-FPM</a>.';
        yellow($heading, $message);
      }
      else {
        $message .= "\n";
        $message .= 'For best results it is recommended to run PHP using <a rel="noreferrer" href="https://www.php.net/manual/en/install.fpm.php">PHP-FPM</a> (preferred) or as an Apache module.';
        red($heading, $message);
      }

      if (! function_exists('opcache_get_configuration') || ! function_exists('opcache_get_status')) {
        throw new Exception('The OPcache extension does not appear to be loaded.  You need to load it with a setting like <code>zend_extension=/full/path/to/opcache.so</code>.');
      }

      $configuration = opcache_get_configuration();
      if (! is_array($configuration)) {
        throw new Exception('opcache_get_configuration() failed');
      }

      $status = opcache_get_status(TRUE);
      if (! is_array($status)) {
        throw new Exception('opcache_get_status() failed');
      }

      if (! $configuration['directives']['opcache.enable']) {
        throw new Exception('The opcode cache does not appear to be enabled.  You need to enable it with the <code>opcache.enable</code> setting.');
      }

      if (! $status['opcache_enabled']) {
        throw new Exception('The opcode cache does not appear to be enabled.');
      }

      // check that the opcache has been running for a while
      $opcache_statistics = $status['opcache_statistics'];
      $last_restart_time = $opcache_statistics['last_restart_time'];
      if ($last_restart_time) {
        $start_time = $last_restart_time;
      }
      else {
        $start_time = $opcache_statistics['start_time'];
      }
      $uptime = time() - $start_time;
      $heading = 'Uptime';
      $message = sprintf('<p>The opcode cache has been up and running for %s.</p>', strong(human_readable_time($uptime)));
      if ($uptime >= 3600) {
        green($heading, $message);
      }
      else {
        $message .= "\n";
        $message .= '<p>Because the opcode cache has not been running for very long, it is likely that some of your .php files have not been accessed and loaded into the cache yet.  You may wish to come back later when you are certain that most of your .php files have been loaded into the opcode cache.</p>';
        if ($uptime >= 60) {
          yellow($heading, $message);
        }
        else {
          red($heading, $message);
        }
      }

      // check memory usage
      $memory_usage = $status['memory_usage'];
      $used_memory = $memory_usage['used_memory'];
      $free_memory = $memory_usage['free_memory'];
      $wasted_memory = $memory_usage['wasted_memory'];
      $total_memory = $used_memory + $free_memory + $wasted_memory;
      $used_percentage = 100 * $used_memory / $total_memory;
      $wasted_percentage = 100 * $wasted_memory / $total_memory;

      // check used memory
      $heading = 'Memory';
      $message = sprintf( '<p>You are using %s of the memory in your opcode cache (%s out of %s bytes).</p>', strong(sprintf('%.1f', $used_percentage) . '%'), strong(number_format($used_memory)), strong(number_format($total_memory)));
      if ($used_percentage < 80) {
        green($heading, $message);
      }
      elseif ($used_percentage < 90) {
        $message .= "\n";
        $message .= '<p>Because you are getting close to running out of space in your opcode cache, you may wish to increase your <code>opcache.memory_consumption</code> setting.</p>';
        yellow($heading, $message);
      }
      else {
        $message .= "\n";
        $message .= '<p>You should increase your <code>opcache.memory_consumption</code> setting to ensure that you do not run out of space in your opcode cache.</p>';
        red($heading, $message);
      }

      // check wasted memory
      $heading = 'Wasted memory';
      if ($wasted_memory == 0) {
        $message = 'You have no wasted memory in your opcode cache.';
        green($heading, $message);
      }
      else {
        define('WASTED_PERCENTAGE_CUTOFF', 5);
        if ($wasted_percentage < WASTED_PERCENTAGE_CUTOFF) {
          $message = sprintf('<p>You have a small amount of wasted memory (%s out of %s bytes, or %s) in your opcode cache.</p>', strong(number_format($wasted_memory)), strong(number_format($total_memory)), strong(sprintf('%.1f', $wasted_percentage) . '%'));
        }
        else {
          $message = sprintf('<p>You have a large amount of wasted memory (%s out of %s bytes, or %s) in your opcode cache.</p>', strong(number_format($wasted_memory)), strong(number_format($total_memory)), strong(sprintf('%.1f', $wasted_percentage) . '%'));
        }
        $message .= "\n";
        $message .= '<p>This is a result of .php files being modified after being stored in the opcode cache.  (The opcode cache cannot free or reuse memory containing old versions of .php files; thus, the memory is wasted until the opcode cache is restarted.)</p>';
        $message .= "\n";
        $message .= '<p>';
        if ($wasted_percentage < WASTED_PERCENTAGE_CUTOFF) {
          $message .= 'It is normal to see wasted memory in the opcode cache after you modify .php files, and it is not a significant issue unless the amount of wasted memory is large.';
          $message .= "\n";
          $message .= 'Nevertheless, you may wish to restart your opcode cache if you want to free this wasted memory.';
        }
        else {
          $message .= 'It is normal to see wasted memory in the opcode cache after you modify .php files, and it is not a significant issue if the amount of wasted memory is small, but it can be a problem if the amount wasted gets too large.';
          $message .= "\n";
          $message .= 'You may wish to restart your opcode cache if you want to free this wasted memory.';
        }
        $message .= '</p>';
        $message .= "\n";
        $message .= 'Note that the opcode cache will restart automatically if the wasted percentage exceeds the setting <code>opcache.max_wasted_percentage</code> (currently this is set to ' . round(100 * $configuration['directives']['opcache.max_wasted_percentage']) . ').';
        if ($wasted_percentage < WASTED_PERCENTAGE_CUTOFF) {
          yellow($heading, $message);
        }
        else {
          red($heading, $message);
        }
      }

      // check interned strings usage
      $interned_strings_usage = $status['interned_strings_usage'];
      $buffer_size = $interned_strings_usage['buffer_size'];
      $used_memory = $interned_strings_usage['used_memory'];
      $used_percentage = 100 * $used_memory / $buffer_size;
      $heading = 'Interned strings';
      $message = sprintf('<p>You are using %s of the memory in your interned strings buffer (%s out of %s bytes).</p>', strong(sprintf('%.1F', $used_percentage) . '%'), strong(number_format($used_memory)), strong(number_format($buffer_size)));
      if ($used_percentage < 80) {
        green($heading, $message);
      }
      elseif ($used_percentage < 90) {
        $message .= "\n";
        $message .= '<p>Because you are getting close to running out of space in your interned strings buffer, you may wish to increase your <code>opcache.interned_strings_buffer</code> setting.</p>';
        yellow($heading, $message);
      }
      else {
        $message .= "\n";
        $message .= '<p>You should increase your <code>opcache.interned_strings_buffer</code> setting to ensure that you do not run out of space in your interned strings buffer.</p>';
        red($heading, $message);
      }

      // check keys
      $opcache_statistics = $status['opcache_statistics'];
      $num_cached_keys = $opcache_statistics['num_cached_keys'];
      $max_cached_keys = $opcache_statistics['max_cached_keys'];
      $used_percentage = 100 * $num_cached_keys / $max_cached_keys;
      $heading = 'Hash table keys';
      $message = 'You are using ' . sprintf('%.1F', $used_percentage) . '% of the hash table keys in your opcode cache.';
      $message = sprintf('<p>You are using %s of the hash table keys in your opcode cache (%s out of %s keys).</p>', strong(sprintf('%.1F', $used_percentage) . '%'), strong(number_format($num_cached_keys)), strong(number_format($max_cached_keys)));
      if ($used_percentage < 80) {
        green($heading, $message);
      }
      elseif ($used_percentage < 90) {
        $message .= "\n";
        $message .= '<p>Because you are getting close to running out of hash table keys, you may wish to increase your <code>opcache.max_accelerated_files</code> setting.</p>';
        yellow($heading, $message);
      }
      else {
        $message .= "\n";
        $message .= '<p>You should increase your <code>opcache.max_accelerated_files</code> setting to ensure that you do not run out of hash table keys.<p>';
        red($heading, $message);
      }
    }
    catch (Exception $e) {
      red('Error', $e->getMessage());
    }

    ?>
  </div>
</body>
</html>
