This is a PHP script for tuning OPcache settings.

To install, simply copy the entire directory to your web server.  Then open the
URL /opcachetuner/ in your web browser.

This program is free software. See the file "LICENSE.txt" for license terms.
